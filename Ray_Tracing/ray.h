#ifndef RAY_H
#define RAY_H

class ray {
    public:
        ray() {}

        ray(const point3& origin, const vec3& direction) : origin(origin), direction(direction), time(0) {}

        ray(const point3& origin, const vec3& direction, double time) : origin(origin), direction(direction), time(time) {}

        const point3& orig() const { return origin; }
        const vec3& direc() const { return direction; }

        double tm() const { return time; }

        point3 at(double t) const {
            return origin + t * direction;
        }

    private:
        point3 origin;
        vec3 direction;
        double time;
};

#endif
